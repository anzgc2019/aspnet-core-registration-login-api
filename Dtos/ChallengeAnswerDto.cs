namespace WebApi.Dtos
{
    public class ChallengeAnswerDto
    {
        public string Submission { get; set; }
    }
}