namespace WebApi.Dtos
{
    public class TeamDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}