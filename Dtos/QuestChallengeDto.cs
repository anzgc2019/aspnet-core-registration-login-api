using WebApi.Entities;

namespace WebApi.Dtos
{
    public class QuestChallengeDto
    {
        // from AssignedChallenge
        public int AssignedChallengeId { get; set; }
        public bool Active { get; set; }
        public string SubmittedAnswer { get; set; }
        public bool Completed { get; set; }
        public int Score { get; set; }
        public int BasicPoints { get; set; }
        public int BonusPoints { get; set; }
        public int PenaltyPoints { get; set; }

        // from Challenge
        public string Title { get; set; }
        public string Detail { get; set; }
        public string MapReference { get; set; }
        public int OfferedPoints { get; set; }
        public int MaxAttempts { get; set; }
        public int TimeLimitSeconds { get; set; }
        public int ChallengeType { get; set; }

        public QuestChallengeDto(AssignedChallenge ac, Challenge c, bool completed) : this(ac, c)
        {
            Completed = completed;
        }

        public QuestChallengeDto(AssignedChallenge ac, Challenge c)
        {
            AssignedChallengeId = ac.Id;
            Active = ac.Active;
            Completed = ac.Completed;
            Score = ac.Score;
            SubmittedAnswer = ac.SubmittedAnswer;
            BonusPoints = ac.BonusPoints;
            PenaltyPoints = ac.PenaltyPoints;

            Title = c.Title;
            Detail = c.Detail;
            MapReference = c.MapReference;
            OfferedPoints = c.BasicPoints;
            MaxAttempts = c.MaxAttempts;
            TimeLimitSeconds = c.TimeLimitSeconds;
            ChallengeType = c.ChallengeType;
        }

    }
}