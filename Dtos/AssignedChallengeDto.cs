namespace WebApi.Dtos
{
    public class AssignedChallengeDto
    {
        public int Id { get; set; }
        public int ChallengeId { get; set; }
        public int TeamId { get; set; }

        public int BasicPoints { get; set; }
        public int BonusPoints { get; set; }
        public int PenaltyPoints { get; set; }

        public bool Active { get; set; }
        public string SubmittedAnswer { get; set; }
        public bool Completed { get; set; }
        public int Score { get; set; }
    }
}