namespace WebApi.Dtos
{
    public class ChallengeResponseDto
    {
        public QuestChallengeDto Result { get; set; }
        public QuestChallengeDto NextChallenge { get; set; }
        public QuestStatsDto Stats { get; set; }

        public ChallengeResponseDto()
        {            
        }

        public ChallengeResponseDto(QuestChallengeDto result, QuestStatsDto stats)
        {
            Result = result;
            Stats = stats;
            NextChallenge = null;
        }

        public ChallengeResponseDto(QuestChallengeDto result, QuestStatsDto stats, QuestChallengeDto next)
        {
            Result = result;
            Stats = stats;
            NextChallenge = next;
        }
    }
}