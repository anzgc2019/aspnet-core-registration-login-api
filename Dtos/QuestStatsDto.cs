namespace WebApi.Dtos
{
    public class QuestStatsDto
    {
        public int Id { get; set; }
        public int BasicPoints { get; set; }
        public int BonusPoints { get; set; }
        public int CollaborationPoints { get; set; }
        public int PenaltyPoints { get; set; }

        public QuestStatsDto()
        {
            BasicPoints = 666;
            BonusPoints = 100;
            CollaborationPoints = 20;
            PenaltyPoints = 50;
        }
    }
}