﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class Team
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }

        public virtual IEnumerable<AssignedChallenge> AssignedChallenges { get; set; }
    }
}
