﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class QuestStats
    {
        [Key]
        public int Id { get; set; }
        public int TeamId { get; set; }
        public virtual Team Team { get; set;}
        public int BasicPoints { get; set; }
        public int BonusPoints { get; set; }
        public int CollaborationPoints { get; set; }
        public int PenaltyPoints { get; set; }
    }
}
