﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public class AssignedChallenge
    {
        [Key]
        public int Id { get; set; }

        public int ChallengeId { get; set; }
        public virtual Challenge Challenge { get; set; }

        public int TeamId { get; set; }
        public virtual Team Team { get; set; }

        public int BasicPoints { get; set; }
        public int BonusPoints { get; set; }
        public int PenaltyPoints { get; set; }

        public bool Active { get; set; }
        public string SubmittedAnswer { get; set; }
        public bool Completed { get; set; }
        public int Score { get; set; }

    }
}
