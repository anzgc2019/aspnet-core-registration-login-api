using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Entities
{
    public class Challenge
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Detail { get; set; }
        public string Answer { get; set; }
        public string MapReference { get; set; }
        public int BasicPoints { get; set; }
        public int BonusPoints { get; set; }
        public int PenaltyPoints {get; set; }
        public int MaxAttempts { get; set; }
        public int TimeLimitSeconds { get; set; }
        public int ChallengeType { get; set; }
        public int NextChallenge { get; set; }
        public int NextChallengeType { get; set; }

        public virtual IEnumerable<AssignedChallenge> AssignedChallenges { get; set; }
    }
}