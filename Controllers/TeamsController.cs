﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.IdentityModel.Tokens.Jwt;
using WebApi.Helpers;
using Microsoft.Extensions.Options;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using WebApi.Services;
using WebApi.Dtos;
using WebApi.Entities;

namespace WebApi.Controllers
{
    [Authorize]
//    [ApiController]
//    [Route("[controller]")]
//    [Route("[controller]/[action]")]
    public class TeamsController : ControllerBase
    {
        private ITeamService _teamService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public TeamsController(
            ITeamService teamService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _teamService = teamService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpGet]
        //[HttpGet(Name = "GetAll")]
        //[AllowAnonymous]
        public IActionResult GetAll()
        {
            //return Ok();
            var teams = _teamService.GetAll();
            var teamDtos = _mapper.Map<IList<TeamDto>>(teams);
            return Ok(teamDtos);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetById(int id)
        {
            var team = _teamService.GetById(id);
            var teamDto = _mapper.Map<TeamDto>(team);
            return Ok(teamDto);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]TeamDto teamDto)
        {
            // map dto to entity and set id
            var team = _mapper.Map<Team>(teamDto);
            team.Id = id;

            try 
            {
                // save 
                _teamService.Update(team);
                return Ok();
            } 
            catch(AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _teamService.Delete(id);
            return Ok();
        }
    }
}
