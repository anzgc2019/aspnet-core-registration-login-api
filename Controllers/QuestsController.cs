﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.IdentityModel.Tokens.Jwt;
using WebApi.Helpers;
using Microsoft.Extensions.Options;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using WebApi.Services;
using WebApi.Dtos;
using WebApi.Entities;

namespace WebApi.Controllers
{
    [Authorize]
//    [ApiController]
//    [Route("[controller]")]
//    [Route("[controller]/[action]")]
    public class QuestsController : ControllerBase
    {
        private ITeamService _teamService;
        private IChallengeService _challengeService;
        private IQuestService _questService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public QuestsController(
            ITeamService teamService,
            IChallengeService challengeService,
            IQuestService questService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _teamService = teamService;
            _challengeService = challengeService;
            _questService = questService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpGet]
        public IActionResult GetAllStats()
        {
            //return Ok();
            var questStats = _questService.GetAllStats();
            var statsDtos = _mapper.Map<IList<QuestStatsDto>>(questStats);
            return Ok(statsDtos);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetStatsById(int id)
        {
            var stats = _questService.GetStatsById(id);
            var statsDto = _mapper.Map<QuestStatsDto>(stats);
            return Ok(statsDto);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetStatsByTeamId(int id)
        {
            var stats = _questService.GetStatsByTeamId(id);
            var statsDto = _mapper.Map<TeamDto>(stats);
            return Ok(statsDto);
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult SubmitAnswer(int id, [FromForm]ChallengeAnswerDto answerDto)
        {
            int acId = id;
            AssignedChallenge ac = _challengeService.GetAssignedChallengeById(acId);
            if (ac == null)
                return BadRequest(new { message = String.Format("No ASSIGNED_CHALLENGE with ID:{0} found.", acId) });
            Challenge c = ac.Challenge;

            AssignedChallenge updatedAssignedChallenge = _challengeService.ProcessChallengeAnswer(acId, answerDto.Submission);
            var stats = _questService.Update(ac.TeamId);
            var statsDto = _mapper.Map<QuestStatsDto>(stats);
            if (updatedAssignedChallenge.Completed == true)
            {
                var newAssignedChallenge = _challengeService.CreateNextAssignedChallenge(acId);
                //return Ok(_mapper.Map<AssignedChallengeDto>(newAssignedChallenge));
                if (newAssignedChallenge != null)
                {
                    var newChallenge = _challengeService.GetById(newAssignedChallenge.ChallengeId);
                    var response = new ChallengeResponseDto(
                            new QuestChallengeDto(updatedAssignedChallenge, c),
                            statsDto, 
                            new QuestChallengeDto(newAssignedChallenge, newChallenge));
                    return Ok(response);
                }
            }

            return Ok(
                        new ChallengeResponseDto(new QuestChallengeDto(updatedAssignedChallenge, c), statsDto)
                      );
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _questService.Delete(id);
            return Ok();
        }
    }
}
