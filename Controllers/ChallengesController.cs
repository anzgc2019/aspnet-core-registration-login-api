﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Options;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using WebApi.Services;
using WebApi.Dtos;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Controllers
{
    [Authorize]
//    [ApiController]
//    [Route("[controller]")]
//    [Route("[controller]/[action]")]
    public class ChallengesController : ControllerBase
    {
        private IChallengeService _challengeService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public ChallengesController(
            IChallengeService challengeService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _challengeService = challengeService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetAll()
        {
            //return Ok();
            var challenges = _challengeService.GetAll();
            var challengeDtos = _mapper.Map<IList<ChallengeDto>>(challenges);
            return Ok(challengeDtos);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetAllByTeamId(int id)
        {
            var questChallengeDtos = _challengeService.GetAllQuestChallengeDtosByTeamId(id);
            return Ok(questChallengeDtos);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetById(int id)
        {
            var challenge =  _challengeService.GetById(id);
            var challengeDto = _mapper.Map<ChallengeDto>(challenge);
            return Ok(challengeDto);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]ChallengeDto challengeDto)
        {
            // map dto to entity and set id
            var challenge = _mapper.Map<Challenge>(challengeDto);
            challenge.Id = id;

            try
            {
                // save 
                _challengeService.Update(challenge);
                return Ok();
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _challengeService.Delete(id);
            return Ok();
        }

    }
}
