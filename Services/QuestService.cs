using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface IQuestService
    {
        IEnumerable<QuestStats> GetAllStats();
        QuestStats GetStatsById(int id);
        QuestStats GetStatsByTeamId(int id);
        QuestStats Update(int id);
        void Delete(int id);
    }

    public class QuestService : IQuestService
    {
        private DataContext _context;

        public QuestService(DataContext context)
        {
            _context = context;
        }

        public IEnumerable<QuestStats> GetAllStats()
        {
            return _context.QuestStats;
        }

        public QuestStats GetStatsById(int id)
        {
            return _context.QuestStats.Find(id);
        }

        public QuestStats GetStatsByTeamId(int id)
        {
            return _context.QuestStats.Where(m => m.TeamId ==  id).FirstOrDefault();
        }

        public QuestStats Update(int id)
        {
            var stats = new QuestStats()
            {
                BasicPoints = 0,
                BonusPoints = 0,
                CollaborationPoints = 0,
                PenaltyPoints = 0
            };
            var assignedChallenges = _context.AssignedChallenges.Where(m => m.TeamId == id);
            foreach( var ac in assignedChallenges )
            {
                stats.BasicPoints += ac.BasicPoints;
                stats.BonusPoints += ac.BonusPoints;
                stats.PenaltyPoints += ac.PenaltyPoints;
            }
            return stats;

            /*
            var QuestStats = _context.QuestStats.Find(id);

            if (QuestStats == null)
                throw new AppException("QuestStats not found");

            // update QuestStats properties

            // TODO

            _context.QuestStats.Update(QuestStats);
            _context.SaveChanges();
            */
        }

        public void Delete(int id)
        {
            var QuestStats = _context.QuestStats.Find(id);
            if (QuestStats != null)
            {
                _context.QuestStats.Remove(QuestStats);
                _context.SaveChanges();
            }
        }

        // private helper methods

    }
}