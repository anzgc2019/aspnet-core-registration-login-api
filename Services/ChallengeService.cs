using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.Dtos;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface IChallengeService
    {
        IEnumerable<Challenge> GetAll();
        IEnumerable<Challenge> GetAllChallengesByTeamId(int id);
        IEnumerable<AssignedChallenge> GetAllAssignedChallengesByTeamId(int id);
        IEnumerable<QuestChallengeDto> GetAllQuestChallengeDtosByTeamId(int id);
        Challenge GetById(int id);
        AssignedChallenge GetAssignedChallengeById(int id);
        // Challenge Create(Challenge Challenge, string password);
        AssignedChallenge ProcessChallengeAnswer(int id, string answer);
        AssignedChallenge CreateNextAssignedChallenge(int id);
        void Update(Challenge Challenge);
        void Delete(int id);
    }

    public class ChallengeService : IChallengeService
    {
        private DataContext _context;

        public ChallengeService(DataContext context)
        {
            _context = context;
        }

        public IEnumerable<Challenge> GetAll()
        {
            return _context.Challenges;
        }

        public IEnumerable<QuestChallengeDto> GetAllQuestChallengeDtosByTeamId(int teamId = 0)
        {
            IList<QuestChallengeDto> challenges = new List<QuestChallengeDto>();

            IEnumerable<AssignedChallenge> candidates = _context.AssignedChallenges.Include(m => m.Challenge).Where(m => m.TeamId == teamId);
            foreach(AssignedChallenge ac in candidates)
            {
                var dto = new QuestChallengeDto(ac, ac.Challenge);
                challenges.Add(dto);
            }

            return challenges;
        }

        public IEnumerable<AssignedChallenge> GetAllAssignedChallengesByTeamId(int teamId = 0)
        {
            return _context.AssignedChallenges.Include(m => m.Challenge).Where(m => m.TeamId == teamId);
        }

        public IEnumerable<Challenge> GetAllChallengesByTeamId(int teamId = 0)
        {
            return _context.AssignedChallenges.Include(m => m.Challenge).Where(m => m.TeamId == teamId).Select(m => m.Challenge);
        }

        public Challenge GetById(int id)
        {
            return _context.Challenges.Find(id);
        }

        public AssignedChallenge GetAssignedChallengeById(int id)
        {
            return _context.AssignedChallenges.Include(m => m.Challenge).Where(m => m.Id == id).First();
        }

        public AssignedChallenge ProcessChallengeAnswer(int ACid, string answer)
        {
            AssignedChallenge ac = GetAssignedChallengeById(ACid);
            ac.SubmittedAnswer = answer;

            if (answer.CloselyMatches(ac.Challenge.Answer))
            {
                ac.Completed = true;
                ac.BasicPoints += ac.Challenge.BasicPoints;
            } else
            {
                ac.Completed = false;
                ac.PenaltyPoints += ac.Challenge.PenaltyPoints;
            }
            ac.Score = calculateScore(ac);
            _context.AssignedChallenges.Update(ac);
            _context.SaveChanges();

            return ac;
        }

        public AssignedChallenge CreateNextAssignedChallenge(int currentChallengeId)
        {
            var ac = GetAssignedChallengeById(currentChallengeId);
            var c = GetById(ac.ChallengeId);

            int nextId = c.NextChallenge > 0 ? c.NextChallenge : getRandomChallenge(ac.TeamId, c.NextChallengeType);
            if (nextId == 0)
                return null;
            else
            {
                var newAC =  new AssignedChallenge() {
                    Id = getNextAvailableAssignedChallengeId(),
                    TeamId = ac.TeamId,
                    ChallengeId = nextId,
                    Active = false,
                    Score = 0,
                    Completed = false
                };
                _context.AssignedChallenges.Add(newAC);
                _context.SaveChanges();
                return newAC;
            }
        }

        private int calculateScore(AssignedChallenge ac)
        {
            return ac.BasicPoints + ac.BonusPoints - ac.PenaltyPoints;
        }
        private int getNextAvailableAssignedChallengeId()
        {
            return _context.AssignedChallenges.Select(m => m.Id).Max() + 1;
        } 
        private int getRandomChallenge( int teamId, int nextChallengeType)
        {
            var exisitng = _context.AssignedChallenges.Where(m => m.TeamId == teamId);
            var candidates = _context.Challenges.Where(m => m.ChallengeType == nextChallengeType).ToList();

            int n = candidates.Count();
            int c = 0;
            Random r = new Random(n);
            int x = r.Next();
            int i;
            while ( c < n )
            {
                i = (x + c) % n;
                var challengeId = candidates.ElementAt(i).Id;
                if( !exisitng.Any(m => m.ChallengeId == challengeId))
                {
                    return challengeId;
                }
                c++;
            }
            return 0;
        }

        /*
        public Challenge Create(Challenge Challenge, string password)
        {
            // validation
            if (string.IsNullOrWhiteSpace(password))
                throw new AppException("Password is required");

            if (_context.Challenges.Any(x => x.Challengename == Challenge.Challengename))
                throw new AppException("Challengename \"" + Challenge.Challengename + "\" is already taken");

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(password, out passwordHash, out passwordSalt);

            Challenge.PasswordHash = passwordHash;
            Challenge.PasswordSalt = passwordSalt;

            _context.Challenges.Add(Challenge);
            _context.SaveChanges();

            return Challenge;
        }
        */
        public void Update(Challenge ChallengeParam)
        {
            var Challenge = _context.Challenges.Find(ChallengeParam.Id);

            if (Challenge == null)
                throw new AppException("Challenge not found");

            if (ChallengeParam.Title != Challenge.Title)
            {
                // Title has changed so check if the new Title is already taken
                if (_context.Challenges.Any(x => x.Title == ChallengeParam.Title))
                    throw new AppException("Challengename " + ChallengeParam.Title + " is already taken");
            }

            // update Challenge properties
            Challenge.Title = ChallengeParam.Title;
            Challenge.Detail = ChallengeParam.Detail;
            Challenge.Answer = ChallengeParam.Answer;

            _context.Challenges.Update(Challenge);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var Challenge = _context.Challenges.Find(id);
            if (Challenge != null)
            {
                _context.Challenges.Remove(Challenge);
                _context.SaveChanges();
            }
        }

        // private helper methods

    }
}