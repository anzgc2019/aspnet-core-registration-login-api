using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface ITeamService
    {
        IEnumerable<Team> GetAll();
        Team GetById(int id);
        // Team Create(Team Team, string password);
        void Update(Team Team);
        void Delete(int id);
    }

    public class TeamService : ITeamService
    {
        private DataContext _context;

        public TeamService(DataContext context)
        {
            _context = context;
        }

        public IEnumerable<Team> GetAll()
        {
            return _context.Teams;
        }

        public Team GetById(int id)
        {
            return _context.Teams.Find(id);
        }

        /*
        public Team Create(Team Team, string password)
        {
            // validation
            if (string.IsNullOrWhiteSpace(password))
                throw new AppException("Password is required");

            if (_context.Teams.Any(x => x.Teamname == Team.Teamname))
                throw new AppException("Teamname \"" + Team.Teamname + "\" is already taken");

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(password, out passwordHash, out passwordSalt);

            Team.PasswordHash = passwordHash;
            Team.PasswordSalt = passwordSalt;

            _context.Teams.Add(Team);
            _context.SaveChanges();

            return Team;
        }
        */
        public void Update(Team TeamParam)
        {
            var Team = _context.Teams.Find(TeamParam.Id);

            if (Team == null)
                throw new AppException("Team not found");

            if (TeamParam.Title != Team.Title)
            {
                // Title has changed so check if the new Title is already taken
                if (_context.Teams.Any(x => x.Title == TeamParam.Title))
                    throw new AppException("Teamname " + TeamParam.Title + " is already taken");
            }

            // update Team properties
            Team.Title = TeamParam.Title;

            _context.Teams.Update(Team);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var Team = _context.Teams.Find(id);
            if (Team != null)
            {
                _context.Teams.Remove(Team);
                _context.SaveChanges();
            }
        }

        // private helper methods

    }
}