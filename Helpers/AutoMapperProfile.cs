using AutoMapper;
using WebApi.Dtos;
using WebApi.Entities;

namespace WebApi.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<AssignedChallenge, AssignedChallengeDto>();
            CreateMap<AssignedChallengeDto, AssignedChallenge>();
            CreateMap<Challenge, ChallengeDto>();
            CreateMap<ChallengeDto, Challenge>();
            CreateMap<QuestStats, QuestStatsDto>();
            CreateMap<QuestStatsDto, QuestStats>();
            CreateMap<Team, TeamDto>();
            CreateMap<TeamDto, Team>();
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
        }
    }
}