﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
// using JsonNet.PrivateSettersContractResolvers;
using WebApi.Entities;
using WebApi.Helpers;

public static class Seeder
{
    public static void SeedData(IApplicationBuilder app)
    {
        var challengeData = System.IO.File.ReadAllText(@"./SeedData/challenges.table.json");
        Seeder.SeedChallenges(challengeData, app.ApplicationServices);

        var teamData = System.IO.File.ReadAllText(@"./SeedData/teams.table.json");
        Seeder.SeedTeams(teamData, app.ApplicationServices);

        var assignedChallengeData = System.IO.File.ReadAllText(@"./SeedData/assignedchallenges.table.json");
        Seeder.SeedAssignedChallenges(assignedChallengeData, app.ApplicationServices);
    }

    public static void SeedChallenges(string jsonData,
                              IServiceProvider serviceProvider)
    {
        JsonSerializerSettings settings = new JsonSerializerSettings
        {
            // ContractResolver = new PrivateSetterContractResolver()
        };
        List<Challenge> challenges =
         JsonConvert.DeserializeObject<List<Challenge>>(
           jsonData, settings);
        using (
         var serviceScope = serviceProvider
           .GetRequiredService<IServiceScopeFactory>().CreateScope())
        {
            var context = serviceScope
                          .ServiceProvider.GetService<DataContext>();
            if (!context.Challenges.Any())
            {
                context.AddRange(challenges);
                context.SaveChanges();
            }
        }
    }

    public static void SeedTeams(string jsonData,
                              IServiceProvider serviceProvider)
    {
        JsonSerializerSettings settings = new JsonSerializerSettings
        {
            // ContractResolver = new PrivateSetterContractResolver()
        };
        List<Team> teams =
         JsonConvert.DeserializeObject<List<Team>>(
           jsonData, settings);
        using (
         var serviceScope = serviceProvider
           .GetRequiredService<IServiceScopeFactory>().CreateScope())
        {
            var context = serviceScope
                          .ServiceProvider.GetService<DataContext>();
            if (!context.Teams.Any())
            {
                context.AddRange(teams);
                context.SaveChanges();
            }
        }
    }

    public static void SeedAssignedChallenges(string jsonData,
                              IServiceProvider serviceProvider)
    {
        JsonSerializerSettings settings = new JsonSerializerSettings
        {
            // ContractResolver = new PrivateSetterContractResolver()
        };
        List<AssignedChallenge> assignedChallenges =
         JsonConvert.DeserializeObject<List<AssignedChallenge>>(
           jsonData, settings);
        using (
         var serviceScope = serviceProvider
           .GetRequiredService<IServiceScopeFactory>().CreateScope())
        {
            var context = serviceScope
                          .ServiceProvider.GetService<DataContext>();
            if (!context.AssignedChallenges.Any())
            {
                context.AddRange(assignedChallenges);
                context.SaveChanges();
            }
        }
    }
}