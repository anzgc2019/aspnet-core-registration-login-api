using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using WebApi.Entities;

namespace WebApi.Helpers
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Challenge> Challenges { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<AssignedChallenge> AssignedChallenges { get; set; }
        public DbSet<QuestStats> QuestStats { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Challenge Keys
            modelBuilder.Entity<Challenge>()
                .HasKey(k => k.Id);
            modelBuilder.Entity<Challenge>()
                .Property(m => m.Id).UseSqlServerIdentityColumn();

            // Team Keys
            modelBuilder.Entity<Team>()
                .HasKey(k => k.Id);
            modelBuilder.Entity<Team>()
                .Property(m => m.Id).UseSqlServerIdentityColumn();

            // AssignedChallenge Keys
            modelBuilder.Entity<AssignedChallenge>()
                .HasKey(k => k.Id);
            modelBuilder.Entity<AssignedChallenge>()
                .HasKey(ac => new { ac.ChallengeId, ac.TeamId });

            modelBuilder.Entity<AssignedChallenge>()
                .Property(m => m.Id).UseSqlServerIdentityColumn();

            modelBuilder.Entity<AssignedChallenge>()
                .HasOne(ac => ac.Challenge)
                .WithMany(c => c.AssignedChallenges)
                .HasForeignKey(ac => ac.ChallengeId);
            modelBuilder.Entity<AssignedChallenge>()
                .HasOne(ac => ac.Team)
                .WithMany(t => t.AssignedChallenges)
                .HasForeignKey(ac => ac.TeamId);

            // Quest Keys
            modelBuilder.Entity<QuestStats>()
                .HasKey(k => k.Id);
        }
    }
}