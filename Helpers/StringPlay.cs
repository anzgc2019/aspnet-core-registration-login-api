﻿
using System;
using System.Text.RegularExpressions;

public static class StringPlay
{
    public static bool CloselyMatches(this string source, string toCheck)
    {
        return source != null && toCheck != null &&
                (source.IndexOf(toCheck, StringComparison.OrdinalIgnoreCase) >= 0 ||
                Regex.Replace(source, @"\s+", "").IndexOf(Regex.Replace(toCheck, @"\s+", ""), StringComparison.OrdinalIgnoreCase) >= 0
                );
    }

}